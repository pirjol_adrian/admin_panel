package com.example.admin_panel.controller;

import com.example.admin_panel.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import static com.example.admin_panel.service.UserService.curentUser;

@RestController
@Controller
public class HomeController {

    @GetMapping("/home")
    public String home(){
        return "This is Home Page";
    }
    @GetMapping("/dashboard")
    public ModelAndView dashboard(){
        ModelAndView modelAndView = new ModelAndView("dashboard");
        modelAndView.addObject("currentUser", curentUser());
        return modelAndView;
    }
    @GetMapping("/admin")
    public String admin(){
        return "This is Admin Page";
    }

    @GetMapping("/index")
    public String index(){
        return "This is Index Page";
    }

    @GetMapping("/blank")
    public ModelAndView blank(){
        ModelAndView modelAndView = new ModelAndView("blankPage");
        modelAndView.addObject("currentUser", new User("Adrian","ADMINTEST"));
        return modelAndView;
    }



}
