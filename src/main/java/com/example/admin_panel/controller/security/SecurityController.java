package com.example.admin_panel.controller.security;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static com.example.admin_panel.service.UserService.curentUser;

@Controller
public class SecurityController {
    @GetMapping("/login")
    public String login() {
        return "login";
    }
    @GetMapping("/forgot")
    public String forgot(){
        return "forgotPassword";
    }

    @GetMapping("/loginError")
    public String loginError(){
        return "login-error";
    }

}
