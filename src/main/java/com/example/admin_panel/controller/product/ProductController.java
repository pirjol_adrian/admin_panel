package com.example.admin_panel.controller.product;

import com.example.admin_panel.model.Product;
import com.example.admin_panel.model.User;
import com.example.admin_panel.model.UserProduct;
import com.example.admin_panel.repository.ProductRepository;
import com.example.admin_panel.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManagerFactory;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.example.admin_panel.service.UserService.curentUser;
import static java.time.LocalDateTime.now;

@RestController
@Controller
public class ProductController {

    @Autowired
    private ProductRepository productRepository;
    private final ProductService productService;
    List<Product> listProductTemp = new ArrayList<>();

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/listProducts")
    public ModelAndView inventar() {
        ModelAndView modelAndView = new ModelAndView("product/listProducts");
        modelAndView.addObject("currentUser", curentUser());
        List<Product> listProducts = productRepository.findAll();
        for (Product product : listProducts) {
            Set<UserProduct> userProductSet = product.getUserProductSet();
            for(UserProduct userProduct : userProductSet){
                modelAndView.addObject("products", listProducts);
                modelAndView.addObject("userProduct", userProduct);
                modelAndView.addObject("date", userProduct.getLocalDateTime());
            }
        }
        return modelAndView;
    }

//===========================

    @PostMapping("/addProduct")
    public ModelAndView createProduct(@ModelAttribute(name = "productForm") Product product) {
        ModelAndView modelAndView = new ModelAndView("product/addProduct");
        modelAndView.addObject("currentUser", curentUser());
        modelAndView.addObject("productTemp", listProductTemp);
        listProductTemp.add(product);
        return modelAndView;
    }

    @GetMapping("/saveProducts")
    public ModelAndView saveProductList() {
        ModelAndView modelAndView = new ModelAndView("dashboard");
        modelAndView.addObject("currentUser", curentUser());
        Set<UserProduct> userProductSet = new HashSet<>();
        for (Product product : listProductTemp) {
            UserProduct userProduct = new UserProduct(curentUser(),product);
            userProductSet.add(userProduct);
            product.setUserProductSet(userProductSet);
            productRepository.save(product);
        }
        listProductTemp.clear();
        userProductSet.clear();
        return modelAndView;
    }


    @GetMapping("/addProduct")
    public ModelAndView getProductForm(ModelMap modelmap) {
        modelmap.addAttribute("productForm", new Product());
        modelmap.addAttribute("currentUser", curentUser());
        modelmap.addAttribute("action", "create");
        return new ModelAndView("product/addProduct");
    }


}

