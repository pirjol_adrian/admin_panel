package com.example.admin_panel.controller.user;

import com.example.admin_panel.model.User;
import com.example.admin_panel.repository.UserRepository;
import com.example.admin_panel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/register")
    public ModelAndView getRegisterForm(ModelMap modelmap) {
        modelmap.addAttribute("createUserForm", new User());
        return new ModelAndView("register");
    }
    @PostMapping("/register")
    public String register(@ModelAttribute("createUserForm") User user) {
        if(user.getRole()==null){
            user.setRole("CLIENT");
        }
        userService.createUser(user);
        return "redirect:/login";
    }
}
