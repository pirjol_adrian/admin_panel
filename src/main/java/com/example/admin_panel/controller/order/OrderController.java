package com.example.admin_panel.controller.order;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import static com.example.admin_panel.service.UserService.curentUser;

@RestController
@Controller
public class OrderController {
    @GetMapping("/addOrder")
    public ModelAndView addOrder(){
        ModelAndView modelAndView = new ModelAndView("order/addOrder");
        modelAndView.addObject("currentUser", curentUser());
        return modelAndView;
    }
    @GetMapping("/listOrders")
    public ModelAndView listOrders(){
        ModelAndView modelAndView = new ModelAndView("order/listOrders");
        modelAndView.addObject("currentUser", curentUser());
        return modelAndView;
    }
}
