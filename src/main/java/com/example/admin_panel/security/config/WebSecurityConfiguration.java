package com.example.admin_panel.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider provider
                 = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService);
        provider.setPasswordEncoder(new BCryptPasswordEncoder());
        return  provider;
    }
    public void configure(WebSecurity web) throws Exception {
        // Ignoră resursele statice și alte resurse publice
        web.ignoring().antMatchers("/static/**", "/public/**",
                "/assets/**", "/vendors/**", "/css/**");
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
//                .antMatchers( "/admin").hasAuthority("ADMIN")
//                .antMatchers("/home").hasAuthority("USER")

                .antMatchers().hasAuthority("ADMIN")
                .antMatchers("/dashboard","/addOrder","/addProduct").hasAnyAuthority("ADMIN", "USER")
                .antMatchers("/home").hasAuthority("CLIENT")
                //
                .antMatchers( "../css/**", "../js/**", "../image/**", "../video/**").permitAll()
                .antMatchers("/register","/index","/loginPage","/forgot",
                        "/loginError,","/login","/blank").permitAll()
                //

                .anyRequest().authenticated()
                .and()

                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/dashboard")//default succesfull page
                .failureUrl("/loginError")

                .and()

                .logout()
                .logoutSuccessUrl("/login")
                .and()

                .csrf().disable();
    }
    @Bean
    public PasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }
}
