package com.example.admin_panel.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "addresses")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    private long id;
    @Column(name = "region")
    private String region;
    @Column(name = "city")
    private String city;
    @Column(name = "street")
    private String street;
    @Column(name = "number_street")
    private String numberStreet;
    @Column(name = "number_build")
    private String numberBuild;
    @Column(name = "level")
    private String level;
    @Column(name = "apartment")
    private String apartment;
    @Column(name = "date_register")
    private LocalDateTime dateRegister;

}
