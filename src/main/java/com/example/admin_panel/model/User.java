package com.example.admin_panel.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "cnp")
    private String cnp;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "email")
    private String email;
    @Column(name = "role")
    private String role;
    @Column(name = "date_register")
    private LocalDateTime dateRegister;


    @ToString.Exclude
    @OneToMany(mappedBy = "user")
    private Set<UserProduct> userProductSet = new HashSet<>();

    public User(String name, String role) {
        this.name = name;
        this.role = role;
    }


}
