package com.example.admin_panel.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;

@Entity
@Table(name = "users_products")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_product_id")
    private long userProductId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "local_dat_time")
    private LocalDateTime localDateTime;

    public UserProduct(User user, Product product) {
        this.user = user;
        this.product = product;
        this.localDateTime= now();

    }

}
