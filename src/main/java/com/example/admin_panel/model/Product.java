package com.example.admin_panel.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "products")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description", length = 10000)
    private String description;
    @Lob
    @Column(name = "photo", columnDefinition = "MEDIUMBLOB")
    private String photo;
    @Column(name = "buy_price")
    private String buyPrice;
    @Column(name = "sell_price")
    private String sellPrice;
    @Column(name = "quantity")
    private String quantity;

    @ToString.Exclude
    @OneToMany(mappedBy = "product", cascade = {CascadeType.ALL})
    private Set<UserProduct> userProductSet = new HashSet<>();




}
