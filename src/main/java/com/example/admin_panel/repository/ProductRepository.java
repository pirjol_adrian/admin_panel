package com.example.admin_panel.repository;

import com.example.admin_panel.model.Product;
import com.example.admin_panel.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository  extends JpaRepository<Product,Long> {

    public Product findByName(String name);
}
